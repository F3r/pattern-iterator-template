package iterator;

public class TestIterator implements Iterator {

    private int index;
    private Object list[];

    public TestIterator(Object[] list) {
        this.list = list;
        this.index = 0;
    }

    @Override
    public boolean hasNext() {
        if(index < list.length){
            return true;
        }
        return false;
    }

    @Override
    public Object next() {
        if(this.hasNext()){
            return list[index++];
        }
        return null;
    }
}
