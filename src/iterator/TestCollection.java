package iterator;

public class TestCollection implements Container {

    private Object list[] = {"name 1","name 2","name 3"};

    @Override
    public Iterator getIterator() {
        return new TestIterator(list);
    }

}
