package iterator;


public class main {

    public static void main(String[] args) {

        TestCollection testCollection = new TestCollection();
        for(Iterator iter = testCollection.getIterator(); iter.hasNext();){
            String name = (String)iter.next();
            System.out.println("Name : " + name);
        }

    }
}
