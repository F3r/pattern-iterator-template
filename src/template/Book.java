package template;

public class Book extends Product {

    private String author;
    private String title;

    public Book(String code, Integer price, String author, String title) {
        super(code, price);
        this.author = author;
        this.title = title;
    }

    @Override
    public String getExtraData() {
        return author + " " + title;
    }
}
