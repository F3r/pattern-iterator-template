package template;

public class CD extends Product {

    private String albumName;
    private String interpreter;

    public CD(String code, Integer price, String albumName, String interpreter) {
        super(code, price);
        this.albumName = albumName;
        this.interpreter = interpreter;
    }

    @Override
    public String getExtraData() {
        return albumName + " " + interpreter;
    }
}
