package template;

public abstract class Product {

    private String code;
    private Integer price;

    public Product(String code, Integer price) {
        this.code = code;
        this.price = price;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public abstract String getExtraData();
}
